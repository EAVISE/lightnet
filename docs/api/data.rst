Data
====
.. automodule:: lightnet.data

Check out the `tutorial <../notes/02-A-basics.html#Pre-processing-pipeline>`_ to learn how to use these operators.

----

Preprocessing
-------------
All pre-processing operators can work with PIL/Pillow images, OpenCV Numpy arrays or PyTorch Tensors (should be normalized float tensors between 0-1).
The pre-processing that works with annotations, expects brambox dataframes.

In order to simplify the process of adding new transformation, a few base classes have been created.
Each pre-processing function in lightnet is a subclass of one of these 3 classes:

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: allmemberinit-template.rst

   lightnet.data.transform.ImageTransform
   lightnet.data.transform.ImageAnnoTransform
   lightnet.data.transform.AnnoTransform

Fit
~~~
These transformation modify the image and annotation data to fit a certain input dimension and as such are all :class:`~lightnet.data.transform.ImageAnnoTransform`. |br|
You can pass the required dimensions directly to these classes, or you can pass a dataset object which will be used to get the dimensions from.
The latter only works with :class:`~lightnet.data.Dataset` and allows to change the required dimensions per batch.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.Crop
   lightnet.data.transform.Letterbox
   lightnet.data.transform.Pad
   lightnet.data.transform.Rescale
   lightnet.data.transform.FitAnno

Augmentation
~~~~~~~~~~~~
These transformations allow you to augment your data, allowing you to train on more varied data without needing to fetch more.
Some of these transformations only modify the image data (usually color modifiers), others also need to modify the annotations and are thus multi-transforms.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.RandomFlip
   lightnet.data.transform.RandomHSV
   lightnet.data.transform.RandomJitter
   lightnet.data.transform.RandomRotate


----

Postprocessing
--------------

GetBoxes
~~~~~~~~
These operators allow you to convert various network output to one of the common bounding box tensor formats:

.. math::
   HBB_{<num\_boxes \, \times \, 7>} &=
   \begin{bmatrix}
      batch\_num, x_{c}, y_{c}, width, height, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, confidence, class\_id \\
      ...
   \end{bmatrix}

   OBB_{<num\_boxes \, \times \, 8>} &=
   \begin{bmatrix}
      batch\_num, x_{c}, y_{c}, width, height, angle, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, angle, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, angle, confidence, class\_id \\
      ...
   \end{bmatrix}

   Mask_{<num\_boxes \, \times \, (7+M)>} &=
   \begin{bmatrix}
      batch\_num, x_{c}, y_{c}, width, height, mask_coef_0, mask_coef_1, ..., mask_coef_M, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, mask_coef_0, mask_coef_1, ..., mask_coef_M, confidence, class\_id \\
      batch\_num, x_{c}, y_{c}, width, height, mask_coef_0, mask_coef_1, ..., mask_coef_M, confidence, class\_id \\
      ...
   \end{bmatrix}

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.GetCornerBoxes
   lightnet.data.transform.GetAnchorBoxes
   lightnet.data.transform.GetMultiScaleAnchorBoxes
   lightnet.data.transform.GetOrientedAnchorBoxes
   lightnet.data.transform.GetMultiScaleOrientedAnchorBoxes
   lightnet.data.transform.GetMaskedAnchorBoxes
   lightnet.data.transform.GetMultiScaleMaskedAnchorBoxes
   lightnet.data.transform.GetMasks

Filtering
~~~~~~~~~
The following classes allow you to filter output bounding boxes based on some criteria. |br|
They can work on the lightnet common bounding box `tensor format <#getboxes>`_ or on a brambox dataframe. 

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.NMS
   lightnet.data.transform.NMSSoft
   lightnet.data.transform.NMSSoftFast

Reverse Fit
~~~~~~~~~~~
These operations cancel the `fit pre-processing <#fit>`_ operators and can only work on brambox dataframes.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.ReverseCrop
   lightnet.data.transform.ReverseLetterbox
   lightnet.data.transform.ReversePad
   lightnet.data.transform.ReverseRescale

Brambox
~~~~~~~
Brambox related post-processing operators.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.data.transform.TensorToBrambox
   lightnet.data.transform.PolygonizeMask

----

Others
------
Some random classes and functions that are used in the data subpackage.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: allmember-template.rst

   lightnet.data.transform.Compose
   lightnet.data.Dataset
   lightnet.data.DataLoader
   lightnet.data.brambox_collate


.. include:: /links.rst
