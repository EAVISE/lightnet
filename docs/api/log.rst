Logging
=======
This package use the standard python logging module to provide some insightfull logging information.
This page gives a basic introduction on how to use it.
For more information on loggers, you can take a look at the `official documentation`_.

In order to have nicer formatting of logging messages, you need a StreamHandler.
You can `create your own <basicconfig_>`_, `use an existing one <richhandler_>`_ or call :func:`~lightnet.create_stream_handler`.
Additionally, the lightnet package provides the :func:`~lightnet.create_file_handler` function to write lightnet log information to a file.

When using the lightnet StreamHandler, there are 2 ways to set the logging level. |br|
The first is by using the ``LN_LOGLVL`` environment variable.
Setting this environment variable before running a lightnet script will force lightnet to filter out messages up to that specified level. |br|
The second method is to call the ``setLevel()`` method on the returned StreamHandler from :func:`~lightnet.create_stream_handler`.


.. rubric:: API
.. autofunction:: lightnet.create_stream_handler
.. autofunction:: lightnet.create_file_handler


.. include:: /links.rst
.. _official documentation: https://docs.python.org/3/library/logging.html
.. _basicconfig: https://docs.python.org/3/library/logging.html#logging.basicConfig
.. _richhandler: https://rich.readthedocs.io/en/stable/logging.html
