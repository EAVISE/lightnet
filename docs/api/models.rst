Models
======
.. automodule:: lightnet.models


Networks
--------
Classification
~~~~~~~~~~~~~~
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.models.Alexnet
   lightnet.models.Darknet
   lightnet.models.Darknet19
   lightnet.models.Darknet53
   lightnet.models.MobileDarknet19
   lightnet.models.MobilenetV1
   lightnet.models.MobilenetV2
   lightnet.models.Resnet18
   lightnet.models.Resnet34
   lightnet.models.Resnet50
   lightnet.models.Resnet101
   lightnet.models.Resnet152
   lightnet.models.VGG11
   lightnet.models.VGG13
   lightnet.models.VGG16
   lightnet.models.VGG19


HBB Detection
~~~~~~~~~~~~~
Horizontal Bounding Box Detection networks

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.models.Cornernet
   lightnet.models.CornernetSqueeze
   lightnet.models.DYolo
   lightnet.models.MobilenetYolo
   lightnet.models.MobileYoloV2
   lightnet.models.MobileYoloV2Upsample
   lightnet.models.ResnetYolo
   lightnet.models.TinyYoloV2
   lightnet.models.TinyYoloV3
   lightnet.models.YoloV2
   lightnet.models.YoloV2Upsample
   lightnet.models.YoloV3
   lightnet.models.YoloFusion
   lightnet.models.Yolt


OBB Detection
~~~~~~~~~~~~~
Oriented Bounding Box Detection networks

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.models.O_DYolo
   lightnet.models.O_YoloV2
   lightnet.models.O_YoloV3
   lightnet.models.O_Yolt


Instance Segmentation
~~~~~~~~~~~~~~~~~~~~~
Instance Segmentation networks

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.models.M_DYolo
   lightnet.models.M_ResnetYolo
   lightnet.models.M_YoloV2
   lightnet.models.M_YoloV3
   lightnet.models.M_Yolt


----


Data
----
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: getitemmember-template.rst

   lightnet.models.BramboxDataset
   lightnet.models.DarknetDataset


.. include:: /links.rst
