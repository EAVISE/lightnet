Network
=======
.. automodule:: lightnet.network

Layer
------

Containers
~~~~~~~~~~
Container modules are used to structure the networks more easily and dont necessarily contain actual computation layers themselves.
You can think of them as more advanced :class:`torch.nn.Sequential` classes.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.network.layer.CSP
   lightnet.network.layer.FeatureExtractor
   lightnet.network.layer.FPN
   lightnet.network.layer.FusionModule
   lightnet.network.layer.FusionSequential
   lightnet.network.layer.HourGlass
   lightnet.network.layer.Parallel
   lightnet.network.layer.ParallelCat
   lightnet.network.layer.ParallelSum
   lightnet.network.layer.Residual
   lightnet.network.layer.SequentialSelect

Convolution
~~~~~~~~~~~~~
These layers are convenience layers that group together one or more convolutions with some other layer (eg. batchnorm, dropout, relu, etc.).

.. Note::
   Most of the convolution layers have an `activation` class argument. |br|
   If you require the `activation` class to get extra parameters, you can use a `lambda` or `functools.partial`:

   >>> conv = ln.network.layer.Conv2dBatchAct(
   ...     3, 32, 3, 1, 1,
   ...     activation=lambda: torch.nn.LeakyReLU(0.1, inplace=True)
   ... )
   >>> print(conv)
   Conv2dBatchAct(3, 32, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), LeakyReLU(negative_slope=0.1, inplace=True))

   >>> import functools
   >>> conv = ln.network.layer.Conv2dDepthWise(
   ...     32, 64, 1, 1, 0,
   ...     activation=functools.partial(torch.nn.ELU, 0.5, inplace=True)
   ... )
   >>> print(conv)
   Conv2dDepthWise(32, 64, kernel_size=(1, 1), stride=(1, 1), padding=(0, 0), ELU(alpha=0.5, inplace=True))

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.network.layer.Conv2dBatchAct
   lightnet.network.layer.Conv2dAct
   lightnet.network.layer.Conv2dDepthWise
   lightnet.network.layer.CornerPool
   lightnet.network.layer.DeformableConv2d
   lightnet.network.layer.InvertedBottleneck
   lightnet.network.layer.ModulatedDeformableConv2d

Pooling
~~~~~~~
These layers perform some kind of specialised pooling operation.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.network.layer.BottomPool
   lightnet.network.layer.LeftPool
   lightnet.network.layer.PaddedMaxPool2d
   lightnet.network.layer.RightPool
   lightnet.network.layer.TopPool

Others
~~~~~~
Miscellaneous layers that don't fit in any other category.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   lightnet.network.layer.Flatten
   lightnet.network.layer.Reorg

----

Backbone
--------
When using these backbones, you get a :class:`torch.nn.Sequential`, which you can use to extract features from images.
If you need intermediate features, you can use our :class:`~lightnet.network.layer.FeatureExtractor` module.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: docmember-template.rst

   lightnet.network.backbone.Alexnet
   lightnet.network.backbone.Cornernet
   lightnet.network.backbone.Darknet
   lightnet.network.backbone.DeformableResnet
   lightnet.network.backbone.MobileDarknet
   lightnet.network.backbone.Mobilenet
   lightnet.network.backbone.ModulatedResnet
   lightnet.network.backbone.Resnet
   lightnet.network.backbone.VGG

----

Head
----
These heads are used on a feature map, which typically comes from a backbone,
and generate output that is expected by the :ref:`api/data:GetBoxes` post-processing.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: docmember-template.rst

   lightnet.network.head.ClassificationConv
   lightnet.network.head.ClassificationFC
   lightnet.network.head.DetectionYoloAnchor
   lightnet.network.head.DetectionOrientedAnchor
   lightnet.network.head.DetectionMaskedAnchor
   lightnet.network.head.DetectionCorner

----

Loss
----
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: forwardmember-template.rst

   lightnet.network.loss.RegionLoss
   lightnet.network.loss.MultiScaleRegionLoss
   lightnet.network.loss.OrientedRegionLoss
   lightnet.network.loss.MultiScaleOrientedRegionLoss
   lightnet.network.loss.MaskedRegionLoss
   lightnet.network.loss.MultiScaleMaskedRegionLoss
   lightnet.network.loss.CornerLoss

----

Module
------
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: allmember-template.rst

   lightnet.network.module.Lightnet
   lightnet.network.module.Darknet
   lightnet.network.module.Fusion


.. include:: /links.rst
