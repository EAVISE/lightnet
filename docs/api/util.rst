Util
====
.. automodule:: lightnet.util


.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst

   lightnet.util.Anchors
   lightnet.util.compute_anchors
   lightnet.util.check_anchors
   lightnet.util.crop_mask
   lightnet.util.cwh_xyxy
   lightnet.util.get_module_device
   lightnet.util.get_module_shape
   lightnet.util.tlwh_xyxy
   lightnet.util.xyxy_cwh
   lightnet.util.iou_cwh
   lightnet.util.iou_tlwh
   lightnet.util.iou_wh
   lightnet.util.iou_bb_cwha
   lightnet.util.iou_bb_quad


.. include:: /links.rst
