Installation
============

First install PyTorch and Torchvision. |br|
You can then install this library by running the following command:

.. code-block:: bash

   # Default installation
   pip install lightnet

   # Training requirements installation mode, which installs brambox and scikit-learn as well
   pip install lightnet[training]

   # If you want to train oriented bounding boxes, you need pgpd as well
   pip install lightnet[training,segment]
   
.. Note::
   This project is python 3.7 and higher so on some systems you might want to use `pip3.7` instead of `pip`. |br|
   Note that you might need to install Pillow or OpenCV as well, but we dont put them in the dependencies, as there are many different versions

.. include:: /links.rst
